import Vue from "vue";

export default {

    async loadData({commit}) {
        let res = await Vue.prototype.$http.get('data.json')
        console.log(res)
        if(res.data){
            commit('setStocks', res.data.stocks)
            commit('setPortfolio', {
                funds: res.data.funds,
                stockPortfolio: res.data.stockPortfolio
            })
        }
    }
}